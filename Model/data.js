const data =  
[
    {
        "id": 1001,
        "category": "Laptop",
        "name": "Acer Laptop",
        "price": 46000,
        "stock": 10
    },
    {
        "id": 1002,
        "category": "Laptop",
        "name": "HP Laptop",
        "price": 52000,
        "stock": 7
    },
    {
        "id": 2001,
        "category": "Pendrive",
        "name": "HP 32 Gb",
        "price": 500,
        "stock": 10
    },
    {
        "id": 2002,
        "category": "Pendrive",
        "name": "Sandisk 64 pendrive",
        "price": 999,
        "stock": 6
    },
    {
        "id": 3001,
        "category": "Mobile",
        "name": "Samsung m21",
        "price": 17999,
        "stock": 5
    },
    {
        "id": 3002,
        "category": "Mobile",
        "name": "Moto G4",
        "price": 13999,
        "stock": 12
    },
    {
        "id": 4001,
        "category": "Camera",
        "name": "Nikon Camera",
        "price": 36999,
        "stock": 7
    },
    {
        "id": 4002,
        "category": "Camera",
        "name": "Asus Camera",
        "price": 25999,
        "stock": 11
    }
]

module.exports = data;