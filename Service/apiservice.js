const express = require('express');
const router = express.Router();
const data = require('../Model/data');
const bodyParser = require('body-parser');


//Home page
router.get('/', (req,res) => {
    res.send("Welcome to Electronics Product Store");
})

//to get all products
router.get('/products', (req,res) => {
    res.json(data);
})


//To get product by its id
router.get('/:id', (req,res) => {
    //prod -- product
    const item = data.some(prod => prod.id === parseInt(req.params.id));
    if(item)
    {
        res.json(data.filter(prod => prod.id === parseInt(req.params.id)));
    }
    else{
        res.status(400).json({msg:'No data found..!'})
    }
});

//Get products by its name
router.get('/:name', (req,res) => {
    const product = data.some(item => item.name === req.params.name);
    if(product)
    {
        res.json(data.filter(item => item.name === req.params.name));
    }
    else{
        res.status(400).json({msg:'No data found..!'})
    }
})


// Filter product by its category name
router.get('/productByCategory/:category', (req,res) => {
    const product = data.some(item => item.category === req.params.category);
    if(product)
    {
        res.json(data.filter(item => item.category === req.params.category));
    }
    else{
        res.status(400).json({msg:'No data found..!'})
    }
})


//POST - create a new product 
router.post('/addProduct', (req,res) => {
    res.send(req.body);
    const newProduct = {
        id: req.body.id,
        category:req.body.category,
        name: req.body.name,
        price: req.body.price,
        stock: req.body.stock
    }

    if(!newProduct.id || !newProduct.category || !newProduct.name || !newProduct.price || !newProduct.stock)
    {
        return res.status(400).json({msg:'Enter all details'})
    }
    //data.push(newProduct);
    res.json({msg:"data added succesfully"},data);
})


//PUT -- Updating existing product based on id
router.put('/updateProduct/:id',(req,res) => {
    const product = data.some(user => user.id === parseInt(req.params.id));
    if(product)
    {
        const updProduct = req.body;
        data.forEach((item => {
            if(item.id === parseInt(req.params.id))
            {
                item.category = updProduct.category ? updProduct.category : item.category;
                item.name = updProduct.name ? updProduct.name : item.name;
                item.price = updProduct.price ? updProduct.price : item.price;
                item.stock = updProduct.stock ? updProduct.stock : item.stock;
                res.json({msg:'Product Updated Sucessfully',item});
            }
        }))
    }
    else
    {
        res.status(400).json({msg:'No data found to update'});
    }
})

//Delete a specific id
router.delete('/product/:id', (req,res) => {
    const product = data.some(item => item.id === parseInt(req.params.id));
    if(product)
    {
        res.json({
            msg:`Product Deleted Sucessfuly`,
            data:data.filter(item => item.id !== parseInt(req.params.id))
        });
    }
    else{
        res.status(400).json({msg:'No data found..!'})
    }
});

module.exports = router;